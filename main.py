import webapp2
import sys

import posts
import writer
import attach

app = webapp2.WSGIApplication([
    ('/',               posts.MainPage),
    ('/posts',          posts.PostsLoader),
    ('/writer',         writer.InterfHandler),
    ('/writer/update',  writer.UpdateHandler),
    ('/attach/upload',  attach.UploadHandler),
    ('/attach/get/(.+)',attach.GetHandler),
    ('/_ah/mail/pub@.*.appspotmail.com',
                        writer.MailUpdateHandler),
], debug=True)
