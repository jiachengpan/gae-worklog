# coding: utf-8
import webapp2

from google.appengine.api import users
from google.appengine.api import mail
from google.appengine.api import app_identity
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler

import logging
import os
import datetime
import json
import re
import mimetypes
import random

from templates import JINJA_ENVIRONMENT
from models import PostRecord

import storage

root_dir = os.path.dirname(os.path.abspath(__file__))

class UploadHandler(webapp2.RequestHandler):
    def post(self):
        file_upload = self.request.POST.getall('file_upload')

        try:
            file_upload = file_upload[0]

            typ, encoding = mimetypes.guess_type(file_upload.filename)
            if not typ: typ = 'application/octet-stream'

            keep_chars = ('_', '-', '.')
            file_name = "%s-%s" % (
                    datetime.datetime.utcnow().strftime('%Y-%m/%d-%H-%M-%S-%f'),
                    "".join([c for c in file_upload.filename if c.isalnum() or c in keep_chars]).rstrip())

            storage.write_file(file_name, file_upload.file.read(), typ)
            resp = {
                    'file_name': self.request.host_url + '/attach/get/' + file_name,
                    }
            self.response.write(json.dumps(resp))
        except:
            return

class GetHandler(webapp2.RequestHandler):
    def get(self, path):
        content, meta = storage.read_file(path)
        if not content or not meta:
            self.response.status = 404
            return
        self.response.content_type = meta.content_type
        self.response.write(content)





