import webapp2

from google.appengine.api import users
import logging
import datetime

from titlecase import titlecase
from templates import JINJA_ENVIRONMENT
from models import PostRecord

JINJA_ENVIRONMENT.filters['titlecase'] = titlecase

class MainPage(webapp2.RequestHandler):
    def get(self):
        months = PostRecord.get_months_with_posts()
        months.sort(reverse=True)
        show_month = self.request.get('show_month', None)

        now = datetime.datetime.utcnow()
        if show_month:
            show_month = datetime.datetime.strptime(show_month, '%Y-%m')
            posts = PostRecord.get_posts_by_month(show_month.year, show_month.month)
        else:
            posts = PostRecord.get_posts_by_time(None)
        user = users.get_current_user()

        user_info = {
                'nickname': '' if not user else user.nickname(),
                'log_url':  users.create_logout_url('/') if user else users.create_login_url('/'),
                'is_admin': users.is_current_user_admin(),
                }
        page_info = {
                'title': show_month.strftime('%B %Y') if show_month else ''
                }

        context = {
                'months':   months,
                'posts':    posts,
                'user':     user_info,
                'page':     page_info,
                'loaded':   show_month,
                }
        template = JINJA_ENVIRONMENT.get_template('templates/posts.html')
        if not user:
            self.response.headers['Cache-Control'] = 'public, max-age=%d' % (60*60)
        self.response.write(template.render(context))

class PostsLoader(webapp2.RequestHandler):
    def get(self):
        start = self.request.get('start', None)

        posts = PostRecord.get_posts_by_time(start)
        user = users.get_current_user()

        user_info = {
                'nickname': '' if not user else user.nickname(),
                'log_url':  users.create_logout_url('/') if user else users.create_login_url('/'),
                'is_admin': users.is_current_user_admin(),
                }
        context = {
                'posts':    posts,
                'user':     user_info,
                }
        template = JINJA_ENVIRONMENT.get_template('templates/posts_data.html')
        self.response.write(template.render(context))

