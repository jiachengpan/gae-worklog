from google.appengine.ext import ndb
from google.appengine.api import memcache
import datetime
import dateutil.parser
import logging

class PostRecordMonthIndex(ndb.Model):
    month = ndb.DateTimeProperty()

class PostRecord(ndb.Model):
    updated_on = ndb.DateTimeProperty(auto_now=True)
    created_on = ndb.DateTimeProperty(auto_now_add=True)
    title      = ndb.StringProperty()
    content    = ndb.StringProperty(indexed=False)
    author     = ndb.StringProperty()

    @classmethod
    def get_post_by_key(cls, key):
        return ndb.Key(urlsafe=key).get()

    @classmethod
    def get_posts_by_month(cls, year, month):
        lowerbound = datetime.datetime(year, month, 1)
        upperbound = lowerbound + datetime.timedelta(days=31)
        upperbound = datetime.datetime(upperbound.year, upperbound.month, 1)

        records = cls.query(ndb.AND(
                    cls.created_on >= lowerbound,
                    cls.created_on < upperbound
                    )).order(-cls.created_on).fetch()
        return records

    @classmethod
    def get_posts_by_time(cls, start_time=None, num=10):
        try:
            upperbound = dateutil.parser.parse(start_time)
        except Exception as e:
            print e
            upperbound = datetime.datetime.utcnow()

        records = cls.query(ndb.AND(
                    cls.created_on < upperbound,
                    )).order(-cls.created_on).fetch(num)
        return records

    @classmethod
    def get_months_with_posts(cls):
        return [m.month for m in PostRecordMonthIndex.query().fetch()]

    @classmethod
    def update_post(cls, title, content, author, key=None):
        post = None
        try:
            if key:
                post = ndb.Key(urlsafe=key).get()

            # new post
            if not key or not post:
                key = ndb.Key(cls, '%s %s %s' % (author, title, datetime.datetime.utcnow().ctime()))
                cls(key=key, title=title, content=content, author=author).put()
                key = key.urlsafe()

                utcnow = datetime.datetime.utcnow()
                month = datetime.datetime(utcnow.year, utcnow.month, 1)
                ret = PostRecordMonthIndex.query(PostRecordMonthIndex.month == month).fetch()
                if not ret:
                    PostRecordMonthIndex(month=month).put()
            else:
                post.title = title
                post.content = content
                post.author = author
                post.put()
        except Exception as e:
            logging.error(str(e))
            return None

        return key

    @classmethod
    def delete_post(cls, key):
        try:
            ndb.Key(urlsafe=key).delete()
            return True
        except:
            return False

