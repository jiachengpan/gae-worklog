# coding: utf-8
import webapp2

from google.appengine.api import users
from google.appengine.api import mail
from google.appengine.api import app_identity
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler

import logging
import os
import datetime
import json
import re

from templates import JINJA_ENVIRONMENT
from models import PostRecord

root_dir = os.path.dirname(os.path.abspath(__file__))

class InterfHandler(webapp2.RequestHandler):
    def get(self):
        post_key = self.request.get('post_key')
        user = users.get_current_user()

	if user:
            nickname = user.nickname()
            logout_url = users.create_logout_url('/')
        else:
            login_url = users.create_login_url('/writer')
            greeting = '<a href="{}">Sign in</a>'.format(login_url)
            self.response.write('<html><body>{}</body></html>'.format(greeting))
            return

        post = None
        if post_key:
            post = PostRecord.get_post_by_key(post_key)

        context = {
                'logout_url':   logout_url,
                'user':         user.nickname(),
                'post_key':     post_key,
                'post_title':   post.title if post else "",
                'post_content': post.content if post else "",
                'page': {
                    'title': 'Update Post'if post else 'New Post',
                    }
                }
        template = JINJA_ENVIRONMENT.get_template('templates/writer.html')
        self.response.write(template.render(context))

class UpdateHandler(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()
        ret = {}
        if not user:
            ret['status'] = 'failed authentication'
        else:
            key = self.request.get('key', None)
            title = self.request.get('title')
            content = self.request.get('content')

            r = PostRecord.update_post(title, content, user.nickname(), key)
            if r:
                ret['status'] = 'ok'
                ret['post_key'] = r
            else:
                ret['status'] = 'failed update'

        self.response.headers['Content-Type'] = 'text/json'
        self.response.write(json.dumps(ret))

class MailUpdateHandler(InboundMailHandler):
    def receive(self, mail_message):
        user = mail_message.sender
        logging.info('Received mail from %s' % user)
        if not re.search('jiacheng', user):
            mail.send_mail(
                    sender='info@%s.appspotmail.com' % app_identity.get_application_id(),
                    to=user,
                    subject='Authentication Failed',
                    body='Sorry, you cannot publish this due to authentication failure.')
            return
        for content_type, body in mail_message.bodies('text/plain'):
            subject = mail_message.subject if 'subject' in dir(mail_message) else ''
            r = PostRecord.update_post(subject, body.decode(), user, None)
            if r:
                mail.send_mail(
                        sender='info@%s.appspotmail.com' % app_identity.get_application_id(),
                        to=user,
                        subject='Publish success!',
                        body='Publish success')
            else:
                mail.send_mail(
                        sender='info@%s.appspotmail.com' % app_identity.get_application_id(),
                        to=user,
                        subject='Publish failed!',
                        body='Publish failed')

class DeleteHandler(webapp2.RequestHandler):
    def post(self):
        pass

