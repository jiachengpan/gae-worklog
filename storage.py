import cloudstorage as gcs
from google.appengine.api import app_identity
from google.appengine.ext import blobstore
import os

write_retry_params = gcs.RetryParams(backoff_factor=1.1)
retry_params = gcs.RetryParams(
        initial_delay=0.2,
        max_delay=5.0,
        backoff_factor=2,
        max_retry_period=15)
gcs.set_default_retry_params(retry_params)

bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
bucket = '/' + bucket_name


def write_file(filename, content, content_type='text/plain'):
    filename = os.path.join(bucket, filename)
    with gcs.open(filename, 'w',
            content_type=content_type,
            retry_params=write_retry_params) as gcs_file:
        gcs_file.write(content)

    blobstore_filename = '/gs' + filename
    return blobstore.create_gs_key(blobstore_filename)

def read_file(filename):
    filename = os.path.join(bucket, filename)
    try:
        gcs_file = gcs.open(filename)
        result = gcs_file.read()
        gcs_file.close()
    except:
        return (None, None)
    return (result, gcs.stat(filename))


