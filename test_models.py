import unittest

# fix the problem when multiple google packages exist -- google.protobuf
import google
reload(google)

from google.appengine.api import memcache
from google.appengine.ext import testbed
from google.appengine.ext import ndb

import datetime

from models import PostRecord

class TestModels(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()

        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

        ndb.get_context().clear_cache()

    def tearDown(self):
        self.testbed.deactivate()

    def testCRUD(self):
        PostRecord.update_post('p0', 'hahaha', 'user1')
        PostRecord.update_post('p0', 'hahaha', 'user1')
        PostRecord.update_post('p1', 'hahaha', 'user1')
        PostRecord.update_post('p2', 'hahaha', 'user1')

        now = datetime.datetime.utcnow()
        ret = PostRecord.get_posts_by_month(now.year, now.month)

        self.assertEqual(len(ret), 3)
        self.assertEqual(PostRecord.delete_post('aaa'), False)

        for r in ret:
            PostRecord.delete_post(r.key.urlsafe())
        ret = PostRecord.get_posts_by_month(now.year, now.month)
        self.assertEqual(len(ret), 0)

        PostRecord.update_post('title', 'hahaha', 'user2')
        PostRecord.update_post('t1', 'hahaha', 'user2')
        PostRecord.update_post('t2', 'hahaha', 'user2')
        ret = PostRecord.get_posts_by_month(now.year, now.month)
        k = ret[0].key.urlsafe()

        updated_content = '-------'
        PostRecord.update_post('title', updated_content, 'user2', k)
        ret = PostRecord.get_posts_by_month(now.year, now.month)
        self.assertEqual(ret[0].content, updated_content)


        now = datetime.datetime.utcnow()
        t = now.isoformat('T')
        rets = []
        for i in range(3):
            ret = PostRecord.get_posts_by_time(t, 1)
            rets.extend(ret)
            t = ret[0].created_on.isoformat('T')
        ret = PostRecord.get_posts_by_time(t, 1)
        self.assertEqual(len(rets), 3)
        self.assertEqual(len(ret), 0)


if __name__ == '__main__':
    unittest.main()

